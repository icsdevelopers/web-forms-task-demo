﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TaskProgressDemo.Startup))]
namespace TaskProgressDemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
