﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System.Diagnostics;
using System.Web;
using System.Threading.Tasks;
using System;

public class Handler : IHttpHandler
{
    public bool IsReusable { get; } = true;

    public void ProcessRequest(HttpContext context)
    {
        Debug.WriteLine("Request received");

        Task.Run(StartDoWork);

        context.Response.ContentType = "text/plain";
        context.Response.Write("Returned back to original context");
    }

    static Task StartDoWork()
    {
        Task.Run(() => LongRunningBackgroundTask(new Progress<string>(handler: progress => Debug.WriteLine(progress))));

        Debug.WriteLine(value: "Long running task started");

        return Task.CompletedTask;
    }

    static async Task LongRunningBackgroundTask(IProgress<string> progress)
    {
        for (int i = 0; i < 10; i++)
        {
            await Task.Delay(500);

            Debug.WriteLine(value: "Progress " + i);
        }

        Debug.WriteLine(value: "Long running task complete");
    }

}

    